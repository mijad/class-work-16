import React from "react"
import "./app.css"
import background from "./img/background.png"

const App = () => {
    return(
        <main >
            <div className="main" style={{backgroundImage:`url(${background})`}}>
                
            </div>
        </main>
    )
}
console.log(background)
export default App